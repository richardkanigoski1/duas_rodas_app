const express = require('express');
const app = express();

app.get('/ola', function (req, res) {
  res.send('Ola!');
});

app.listen(3001, function () {
  console.log('Example app listening on port 3001!');
});
