const axios = require('axios');

export const get = ({ url }) => axios.get(url);

export const remove = ({ url }) => axios.delete(url);

export const post = ({ url, body = {}, headers = {} }) => axios.post(url, body, headers);

export const put = ({ url, body = {}, headers = {} }) => axios.put(url, body, headers);