import Vue from 'vue';
import Router from 'vue-router';
import login from './views/Login.vue';
import OrderList from './views/Order_List.vue';
import cadUser from './views/cad/Cad_User.vue';
import Order from './views/Order.vue';
import EPI from './views/EPI_Confirm.vue';
import cadOrder from './views/cad/Cad_Order.vue';
import cadSector from './views/cad/Cad_Sector.vue';
import cadDefect from './views/cad/Cad_Defect.vue';
import cadMaintenance from './views/cad/Cad_Maintenance_Type.vue';
import cadEquipments from './views/cad/Cad_equipments.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: '/',
    },
    {
      path: '/',
      name: 'Login',
      component: login,
    },
    {
      path: '/order',
      name: 'Listagem',
      component: OrderList,
    },
    {
      path: '/order/:id',
      name: 'Ordem de manutenção',
      component: Order,
    },
    {
      path: '/user',
      name: 'Cadastro de Usuário',
      component: cadUser,
    },
    {
      path: '/epi/:id',
      name: 'Confirmação Epi\'s',
      component: EPI,
    },
    {
      path: '/new_order',
      name: 'Nova Ordem de Manutenção',
      component: cadOrder,
    },
    {
      path: '/sector',
      name: 'Novo Setor',
      component: cadSector,
    },
    {
      path: '/defect',
      name: 'Novo Defeito',
      component: cadDefect,
    },
    {
      path: '/maintenance',
      name: 'Tipo de Manutenção',
      component: cadMaintenance,
    },
    {
      path: '/equipments',
      name: 'Equipamentos',
      component: cadEquipments,
    },
  ],
});
